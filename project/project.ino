#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>

uint8_t getFingerprintEnroll(uint8_t id);


// pin #2 is IN from sensor (GREEN wire)
// pin #3 is OUT from arduino  (WHITE wire)
SoftwareSerial mySerial(2, 3);

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

void setup()  
{
  Serial.begin(9600);
  Serial.println("fingertest");

  // set the data rate for the sensor serial port
  finger.begin(57600);
  
  if (finger.verifyPassword()) {
    Serial.println("Found fingerprint sensor!");
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    while (1);
  }
}

void loop()                     // run over and over again
{
  Serial.println("press s to Scan fingerprint");
  Serial.println("press d to Delete fingerprint");
  Serial.println("press e to enroll fingerprint");
  Serial.println("press v to views fingerprint");
  while (true) {
    while (! Serial.available());
    char c = Serial.read();
    if( c == 's') {
      fingerprint();
      break;
    }
    else if(c == 'd') {
      deletefingerprint();
      break;
    }
    else if(c == 'e') {
      enroll();
      break;
    }  
    else if(c == 'v'){
      viewstemp();
      break;
    }
  }
}

void viewstemp(){
  Serial.println("Program Views Template");
  int id = 0;
  String tempc="";
  while(true){
    while (! Serial.available());
    char c = Serial.read();
    if(c == 'a'){
      for (int finger = 0; finger <= 255; finger++){
         uploadFingerpintTemplate(finger);
      }
       break;
    }
    else if (isdigit(c)) {
      tempc = tempc + c;
      id = tempc.toInt();
      uploadFingerpintTemplate(id);
    }
    else if(c == ';'){
       id = 0;
       tempc="";
    }
    else if(c == 'v'){
      break;
    }
  }
}

void enroll(){
    Serial.println("Type in the ID # you want to save this finger as...");
    uint8_t id = 0;
    while (true) {
      while (! Serial.available());
      char c = Serial.read();
      if(c == 'e') {break;}
      else if (! isdigit(c)) {
        Serial.print("Enrolling ID #");
        Serial.println(id);
        while (!  getFingerprintEnroll(id) );
        break;
      }
      id *= 10;
      id += c - '0';
    }
}

void fingerprint(){
  Serial.println("Scann Your Fingerpring");
  while(true){
    while (! Serial.available()){
      char c = Serial.read();
      if(c == 's') break;
      int check = getFingerprintIDez();
      if(check != -1) break;
      delay(50);
    }
    break;
  }
}

void deletefingerprint(){
  while(true){
      Serial.println("Type in the ID # you want delete...");
      uint8_t id = 0;
      char tempc = ' ';
      while (true) {
        while (! Serial.available());
        char c = Serial.read();
        if(c == 'd') {tempc = c; break;}
        else if(c == 'a'){
          for(int i = 0 ; i <= 255 ; i++){
            deleteFingerprint(i);
          }
          break;
        }
        else if(! isdigit(c)) break;
        id *= 10;
        id += c - '0';
      }
      if(tempc == 'd') break;
      Serial.print("deleting ID #");
      Serial.println(id);
      
      deleteFingerprint(id);
  }
}

uint8_t deleteFingerprint(uint8_t id) {
  uint8_t p = -1;
  
  p = finger.deleteModel(id);

  if (p == FINGERPRINT_OK) {
    Serial.println("Deleted!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not delete in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.print("Unknown error: 0x"); Serial.println(p, HEX);
    return p;
  }   
}

uint8_t getFingerprintEnroll(uint8_t id) {
  uint8_t p = -1;
  Serial.println("Waiting for valid finger to enroll");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.println(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  Serial.println("Remove finger");
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }

  p = -1;
  Serial.println("Place same finger again");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  
  // OK converted!
  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("Prints matched!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    Serial.println("Fingerprints did not match");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
  
  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    Serial.println("Stored!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
}

// returns -1 if failed, otherwise returns ID #
int getFingerprintIDez() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return -1;
  
  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID);  
  Serial.print(" with confidence of "); Serial.println(finger.confidence);
  uploadFingerpintTemplate(finger.fingerID);
  return finger.fingerID; 
}

uint8_t uploadFingerpintTemplate(uint16_t id)
{
 uint8_t p = finger.loadModel(id);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.print("template "); Serial.print(id); Serial.println(" loaded");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    default:
      Serial.print("Unknown error "); Serial.println(p);
      return p;
  }

  // OK success!

  p = finger.getModel();
  switch (p) {
    case FINGERPRINT_OK:
      Serial.print("template "); Serial.print(id); Serial.println(" transferring");
      break;
   default:
      Serial.print("Unknown error "); Serial.println(p);
      return p;
  }
  
  //Template data seems to be 78 bytes long?  This prints out 5 lines of 16 bytes (so there's 2 extra FF bytes at the end)
  for (int count= 0; count < 5; count++)
  {
    for (int i = 0; i < 16; i++)
    {
      Serial.print(mySerial.read(), HEX);
    }
  }
  Serial.println();
}


