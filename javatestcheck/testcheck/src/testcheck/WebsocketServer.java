package testcheck;

import Decoder.BASE64Encoder;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;

public class WebsocketServer {
    public static final int MASK_SIZE = 4;
    public static final int SINGLE_FRAME_UNMASKED = 0x81;
    public ServerSocket serverSocket;
    public Socket socket;

    public void setport(int port) throws IOException{
        serverSocket = new ServerSocket(port);
    }

    public void connect() throws IOException, NoSuchAlgorithmException {
        System.out.println("Listening");
        socket = serverSocket.accept();
        System.out.println("Got connection");
        if(handshake()) {
            listenerThread();
        }
    }

    private boolean handshake() throws IOException, NoSuchAlgorithmException {
        PrintWriter out = new PrintWriter(socket.getOutputStream());
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        HashMap<String, String> keys = new HashMap<>();
        String str;
        //Reading client handshake
        while (!(str = in.readLine()).equals("")) {
            String[] s = str.split(": ");
            System.out.println();
            System.out.println(str);
            if (s.length == 2) {
            keys.put(s[0], s[1]);
            }
        }
        //Do what you want with the keys here, we will just use "Sec-WebSocket-Key"

        String hash;
        try {
            hash = new BASE64Encoder().encode(MessageDigest.getInstance("SHA-1").digest((keys.get("Sec-WebSocket-Key") + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            return false;
        }

        //Write handshake response
        out.write("HTTP/1.1 101 Switching Protocols\r\n"
            + "Upgrade: websocket\r\n"
            + "Connection: Upgrade\r\n"
            + "Sec-WebSocket-Accept: " + hash + "\r\n"
            + "\r\n");
        out.flush();

        return true;
    }

    private byte[] readBytes(int numOfBytes) throws IOException {
        byte[] b = new byte[numOfBytes];
        socket.getInputStream().read(b);
        return b;
    }

    public void listenerThread() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
            try {
                while (true) {
                    System.out.println("Recieved from client: " + reiceveMessage());
                    fingerprint(reiceveMessage());
                }
            } catch (IOException ex) {
            }
            }
        });
        t.start();
    }

    public String reiceveMessage() throws IOException {
        byte[] buf = readBytes(2);
        System.out.println("Headers:");
        convertAndPrint(buf);
        int opcode = buf[0] & 0x0F;
        if (opcode == 8) {
            //Client want to close connection!
            System.out.println("Client closed!");
            socket.close();
            System.exit(0);
            return null;
        } else {
            final int payloadSize = getSizeOfPayload(buf[1]);
            System.out.println("Payloadsize: " + payloadSize);
            buf = readBytes(MASK_SIZE + payloadSize);
            System.out.println("Payload:");
            convertAndPrint(buf);
            buf = unMask(Arrays.copyOfRange(buf, 0, 4), Arrays.copyOfRange(buf, 4, buf.length));
            String message = new String(buf);
            return message;
        }
    }

    private int getSizeOfPayload(byte b) {
        //Must subtract 0x80 from masked frames
        return ((b & 0xFF) - 0x80);
    }

    private byte[] unMask(byte[] mask, byte[] data) {
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) (data[i] ^ mask[i % mask.length]);
        }
        return data;
    }

    private void convertAndPrint(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        System.out.println(sb.toString());
    }
    
    public void brodcast(String mess) throws IOException{
    BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
    byte[] rawData = mess.getBytes();

    int frameCount  = 0;
    byte[] frame = new byte[10];

    frame[0] = (byte) 129;

    if(rawData.length <= 125){
        frame[1] = (byte) rawData.length;
        frameCount = 2;
    }else if(rawData.length >= 126 && rawData.length <= 65535){
        frame[1] = (byte) 126;
        int len = rawData.length;
        frame[2] = (byte)((len >> 8 ) & (byte)255);
        frame[3] = (byte)(len & (byte)255); 
        frameCount = 4;
    }else{
        frame[1] = (byte) 127;
        int len = rawData.length;
        frame[2] = (byte)((len >> 56 ) & (byte)255);
        frame[3] = (byte)((len >> 48 ) & (byte)255);
        frame[4] = (byte)((len >> 40 ) & (byte)255);
        frame[5] = (byte)((len >> 32 ) & (byte)255);
        frame[6] = (byte)((len >> 24 ) & (byte)255);
        frame[7] = (byte)((len >> 16 ) & (byte)255);
        frame[8] = (byte)((len >> 8 ) & (byte)255);
        frame[9] = (byte)(len & (byte)255);
        frameCount = 10;
    }

    int bLength = frameCount + rawData.length;

    byte[] reply = new byte[bLength];

    int bLim = 0;
    for(int i=0; i<frameCount;i++){
        reply[bLim] = frame[i];
        bLim++;
    }
    for(int i=0; i<rawData.length;i++){
        reply[bLim] = rawData[i];
        bLim++;
    }
    System.out.println("Sennding to Client: "+Arrays.toString(reply));
    out.write(reply);
    out.flush();

}
    
    public void fingerprint(String s) throws IOException{
        switch (s) {
            case "Hand Check":
                System.out.println("Command Hand Check");
                Fingerprint.handcheck();
                String rehancheck = new SerialPortReader().ReaddataSerial(12);
                brodcast(rehancheck);
            break;
            case "Scan":
                System.out.println("Command Scan");
                while(true){
                    Fingerprint.ScanFinger();
                    String rescan = new SerialPortReader().ReaddataSerial(12);
                    if(!rescan.equals("EF 01 FF FF FF FF 07 00 03 02 00 0C")){
                        Fingerprint.getimage();
                        break;
                    }
                }
                String rescan = new SerialPortReader().ReaddataSerial(40044);
                brodcast(rescan);
            break;
            case "Info":
                System.out.println("Command Info");
                Fingerprint.confirmhandcheck();
                String reInfo = new SerialPortReader().ReaddataSerial(568);
                brodcast(reInfo);
            break;
            default:
                brodcast("Not Command"); // 125 length
                System.out.println("Not Command");
            break;
        }
    }
}
