package testcheck;

import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class SerialPortReader implements SerialPortEventListener {
    public static SerialPort serialPort;
    
    public String serialconnect(String serailport){
        serialPort = new SerialPort(serailport); 
        try {
            serialPort.openPort();//Open port
            serialPort.setParams(57600, 8, 1, 0);//Set params
            int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
            serialPort.setEventsMask(mask);//Set mask
            serialPort.addEventListener(new SerialPortReader());//Add SerialPortEventListener
            Fingerprint.handcheck();
            String returndata = ReaddataSerial(12);
            return returndata;
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
            return "Error";
        }
    }
    public static void closeport(){
        try {
            serialPort.closePort();//Close serial port
        } catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }
    public String[] FindAllSerialPort(){    // method ค้นหา serail port ทั้งหมดที่เชื่อมต่อคอมอยู่
        String[] portNames = SerialPortList.getPortNames(); // ดึง ลิส ของ serail port มาใส่ใน String array
        return portNames;
    } // จบ method FindAllSerial
    
    public String ReaddataSerial(int num){
        try {
            String data = serialPort.readHexString(num);
            return data;
        } catch (SerialPortException ex) {
            Logger.getLogger(SerialPortReader.class.getName()).log(Level.SEVERE, null, ex);
            return "Error";
        }
    }
    
    public static void WriteDataSerial(byte data){
        try {
            serialPort.writeByte(data);//Write data to port
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }
    
    @Override
    public void serialEvent(SerialPortEvent event) {
        if(event.isRXCHAR()){//If data is available
            if(event.getEventValue() > 0){//Check bytes count in the input buffer
                //Read data, if 10 bytes available 
                //try {
                    System.out.println(event.getEventValue());
                    //System.out.println(serialPort.readHexString());
                //}
                //catch (SerialPortException ex) {
                //    System.out.println(ex);
                //}
            }
        }
    }
}
    

