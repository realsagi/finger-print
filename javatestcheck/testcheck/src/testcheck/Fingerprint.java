package testcheck;

public class Fingerprint {
    public static void getimage(){
        // EF 01 FF FF FF FF 01 00 03 0A 00 0E
        SerialPortReader.WriteDataSerial((byte)0xEF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x03);
        SerialPortReader.WriteDataSerial((byte)0x0A);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x0E);
    }
    public static void ScanFinger(){
        // EF 01 FF FF FF FF 01 00 03 01 00 05
        SerialPortReader.WriteDataSerial((byte)0xEF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x03);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x05);
    }
    public static void handcheck(){
        // EF 01 FF FF FF FF 01 00 07 13 00 00 00 00 00 1B
        SerialPortReader.WriteDataSerial((byte)0xEF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x07);
        SerialPortReader.WriteDataSerial((byte)0x13);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x1B);
    }
    public static void confirmhandcheck(){
        // EF 01 FF FF FF FF 01 00 03 16 00 1A
        SerialPortReader.WriteDataSerial((byte)0xEF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0xFF);
        SerialPortReader.WriteDataSerial((byte)0x01);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x03);
        SerialPortReader.WriteDataSerial((byte)0x16);
        SerialPortReader.WriteDataSerial((byte)0x00);
        SerialPortReader.WriteDataSerial((byte)0x1A);
    }
}
