package testcheck;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Testcheck {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        SerialPortReader serialport = new SerialPortReader();
        String[] allport = serialport.FindAllSerialPort();
        Boolean checkfailconnect = false;
        for (String portName : allport) { 
            String statusconnect = serialport.serialconnect(portName);
            if(statusconnect.equals("EF 01 FF FF FF FF 07 00 03 00 00 0A")){
                System.out.println(portName);
                checkfailconnect = true;
                break;
            }
            else{
                checkfailconnect = false;
            }
        }
        if(checkfailconnect){
            System.out.println("The Fingerprint server is running.");
            WebsocketServer websocketServer = new WebsocketServer();
            websocketServer.setport(8888);
            websocketServer.connect();
        }
        else{
            System.out.println("Error Connect Serial Port");
        }
    }
}
